import React from 'react';
import { Link } from 'react-router-dom';

export default ({ to, title = null, children, ...other }) => {
  const Component = /^https?:\/\//.test(to) ? 'a' : Link;

  if (to.startsWith('#')) to = to.slice(1);
  if (!title) {
    if (to.includes('/types/')) title = 'Type';
    else if (to.includes('/methods/')) title = 'Method';
  }

  if (Component === 'a') other.href = to;
  else other.to = to;

  return (
    <span>
      <Component {...other}>{children}</Component>
      {title && <span className="annotation">{title}</span>}
      <style jsx>{`
        .annotation {
          margin-left: 0.3em;
          padding: 0.2em 0.4em 0.15em;
          border-radius: 0.7em;
          background: #ccccea;
          color: #fff;
          font-size: 0.8em;
          font-weight: 500;
          text-transform: uppercase;
        }
      `}</style>
    </span>
  );
};
