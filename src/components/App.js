import React from 'react';
import { HashRouter as Router, Route, Switch } from 'react-router-dom';

import Styles from '../styles';
import Container from './Container';

import Spinner from './Spinner';
import Header from './Header';
import RightSidebar from './RightSidebar';

import MainPage from '../pages/MainPage';
import NotFoundPage from '../pages/NotFoundPage';
import TypePage from '../pages/TypePage';
import MethodPage from '../pages/MethodPage';
import ArticlePage from '../pages/ArticlePage';

import ScrollToTop from './ScrollToTop';

function App() {
  const [data, setData] = React.useState(null);

  React.useEffect(() => {
    const BASE_URL = 'https://tgbotapi.gitlab.io/schema';
    Promise.all([
      fetch(`${BASE_URL}/types.json`).then(resp => resp.json()),
      fetch(`${BASE_URL}/methods.json`).then(resp => resp.json()),
      fetch(`${BASE_URL}/articles.json`).then(resp => resp.json()),
      fetch(`${BASE_URL}/version.txt`).then(resp => resp.text()),
      fetch(`${BASE_URL}/build_info.json`).then(resp => resp.json()),
    ]).then(values => {
      setData({
        types: values[0],
        methods: values[1],
        articles: values[2],
        botAPIVersion: values[3],
        buildInfo: values[4],
      });
    });
  }, []);

  return (
    <Router>
      <ScrollToTop />
      <div className="app">
        <style jsx>{Styles}</style>
        <Header />
        <Container position="relative">
          {data ? (
            <main>
              <div className="content">
                <Switch>
                  <Route
                    path="/types/:type"
                    render={routeProps => (
                      <TypePage {...routeProps} types={data.types} />
                    )}
                  />
                  <Route
                    path="/methods/:method"
                    render={routeProps => (
                      <MethodPage {...routeProps} methods={data.methods} />
                    )}
                  />
                  <Route
                    path="/articles/:article"
                    render={routeProps => (
                      <ArticlePage {...routeProps} articles={data.articles} />
                    )}
                  />
                  <Route
                    exact
                    path="/"
                    render={routeProps => (
                      <MainPage
                        {...routeProps}
                        articles={data.articles}
                        botAPIVersion={data.botAPIVersion}
                        buildInfo={data.buildInfo}
                      />
                    )}
                  />
                  <Route component={NotFoundPage} />
                </Switch>
              </div>
              <RightSidebar types={data.types} methods={data.methods} />
            </main>
          ) : (
            <div className="spinner-wrapper">
              <Spinner size={8} width={0.5} color="#AAAAAA" />
              <p>Loading database...</p>
              <p className="secondary">
                <i>This can take up to 10 seconds.</i>
              </p>
            </div>
          )}
        </Container>
        {/* language=CSS */}
        <style jsx>{`
          .app {
            height: 100%;
            min-height: 100vh;
          }

          main {
            padding-top: 3rem;
          }

          .content {
            margin-right: 15rem;
          }

          .spinner-wrapper {
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
            height: 100vh;
            text-align: center;
          }
          .spinner-wrapper p {
            margin-top: 2em;
            color: #666;
          }
          .spinner-wrapper p.secondary {
            margin-top: 0em;
            color: #888;
            font-size: 0.9em;
          }
        `}</style>
      </div>
    </Router>
  );
}

export default App;
