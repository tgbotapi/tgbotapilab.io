import React from 'react';
import classNames from 'classnames';
import Spinner from './Spinner';

export default ({
  children,
  primary = false,
  fullWidth = false,
  loading = false,
  ...other
}) => (
  <>
    <button
      className={classNames(
        primary && 'primary',
        fullWidth && 'fullWidth',
        loading && 'loading'
      )}
      disabled={loading}
      {...other}
    >
      {loading ? <Spinner size={'1.5rem'} /> : children}
    </button>
    {/* language=CSS */}
    <style jsx>{`
      button {
        border: none;
        border-radius: 0.25rem;
        padding: 0.5rem 1rem 0.4rem;
        background: #fff;
        color: rgb(100, 100, 200);
        font: inherit;
        font-size: 0.85rem;
        font-weight: 500;
        text-transform: uppercase;
        cursor: pointer;
        box-shadow: inset rgba(100, 100, 200, 0.15) 0 0 0 1px;
        transition: background-color 0.15s ease, color 0.15s ease,
          box-shadow 0.15s ease;
      }
      button.fullWidth {
        width: 100%;
      }
      button.loading {
        padding: 0.47rem 1.5rem;
        font-size: 0;
        opacity: 0.7;
        pointer-events: none;
      }

      button:hover {
        background: rgba(100, 100, 200, 0.1);
      }
      button:active {
        background: rgba(100, 100, 200, 0.3);
        color: rgb(70, 70, 170);
      }
      button:focus {
        outline: none;
      }

      button.primary {
        background: rgb(100, 100, 200);
        color: #fff;
        box-shadow: none;
      }
      button.primary:hover {
        background: rgb(110, 110, 210);
        color: #fff;
      }
      button.primary:active {
        background: rgb(70, 70, 170);
        color: #eee;
      }
    `}</style>
  </>
);
