import React, { useRef } from 'react';
import useMeasure from 'use-measure';

export default ({ collapsed = false, children }) => {
  const ref = useRef(null);
  const { height } = useMeasure(ref);

  return <div ref={ref}>{children}</div>;
}
