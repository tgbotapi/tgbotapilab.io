import React from 'react';

export default ({ children, position = 'initial', ...other }) => (
  <>
    <div {...other}>
      {children}
    </div>
    {/* language=CSS */}
    <style jsx>{`
      div {
        position: ${position};
        width: calc(100% - 2rem * 2);
        max-width: 75rem;
        margin: 0 auto;
      }
    `}</style>
  </>
)
