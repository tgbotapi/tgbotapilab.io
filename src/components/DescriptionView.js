import React from 'react';
import ReactMarkdown from 'react-markdown';
import AnnotatedLink from './AnnotatedLink';

export default ({ description, middleware = a => a }) => (
  <ReactMarkdown
    source={middleware(description.markdown)}
    renderers={{
      link: ({ children, href }) => (
        <AnnotatedLink to={href}>{children}</AnnotatedLink>
      ),
    }}
  />
);
