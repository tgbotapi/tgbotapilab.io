import React from 'react';
import { Link } from 'react-router-dom';
import DescriptionView from './DescriptionView';

export default ({ fields }) =>
  Object.keys(fields).map(fieldName => (
    <div key={fieldName}>
      <h4>
        <code>
          <b>{fieldName}</b>:{' '}
          {/* Here we insert " or " between all elements by inserting it
           * before each element and then slicing it by one */}
          {[]
            .concat(
              ...fields[fieldName].types
                .map(field =>
                  field[0].toUpperCase() === field[0] ? (
                    <Link key={field} to={`/types/${field}`}>
                      {field}
                    </Link>
                  ) : (
                    <span key={field}>{field}</span>
                  )
                )
                .map(e => [<span className="joiner"> or </span>, e])
            )
            .slice(1)}
          {/* {!fields[fieldName].required && (
            <span className="label">(optional)</span>
          )} */}
        </code>
      </h4>
      <div className="description">
        {!fields[fieldName].required && (
          <span className="label">Optional.</span>
        )}
        <DescriptionView
          description={fields[fieldName].description}
          middleware={text => text.replace(/^_Optional_\./, '')}
        />
      </div>

      <style jsx>{`
        h4 code .joiner {
          margin-right: 0.25em;
        }
        .label {
          float: left;
          margin-right: 0.25em;
          color: #888;
        }
      `}</style>
    </div>
  ));
