import React from 'react';
import { Link } from 'react-router-dom';
import DescriptionView from './DescriptionView';

export default ({ fields }) => (
  <table>
    <thead>
      <tr>
        <td>Key</td>
        <td>Types</td>
        <td>Description</td>
      </tr>
    </thead>
    <tbody>
      {Object.keys(fields).map(fieldName => (
        <tr key={fieldName}>
          <td>
            <code>
              <b>{fieldName}</b>
            </code>
          </td>
          <td>
            <code>
              {[]
                .concat(
                  ...fields[fieldName].types
                    .map(field =>
                      field[0].toUpperCase() === field[0] ? (
                        <Link key={field} to={`/types/${field}`}>
                          {field}
                        </Link>
                      ) : (
                        <span key={field}>{field}</span>
                      )
                    )
                    .map(e => [<span className="joiner"> or </span>, e])
                )
                .slice(1)}
            </code>
          </td>
          <td>
            <div className="description">
              {!fields[fieldName].required && (
                <span className="label">Optional.</span>
              )}
              <DescriptionView
                description={fields[fieldName].description}
                middleware={text => text.replace(/^_Optional_\./, '')}
              />
            </div>
          </td>
        </tr>
      ))}
    </tbody>

    <style jsx>{`
      table {
        width: 100%;
        margin: 0 -0.5rem;
        border-collapse: collapse;
        font-size: 0.95em;
      }
      table thead {
        font-weight: 500;
      }
      table tr {
        border-bottom: 1px solid #eee;
      }
      table td {
        padding: 0.2rem 0.5rem;
      }
      table .required-mark {
        color: #f33;
        margin-left: 0.25rem;
      }

      code {
        font-size: 0.95em;
      }

      .label {
        float: left;
        margin-right: 0.25em;
        color: #888;
      }
    `}</style>
  </table>
);
