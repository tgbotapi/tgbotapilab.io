import React, { useState } from 'react';
import MaterialIcon from './MaterialIcon';
import IconButton from './IconButton';
import FieldsTableView from './FieldsTableView';
import FieldsListView from './FieldsListView';
import { useLocalStorage } from '../utils';

export default ({ fields }) => {
  const [viewType, setViewType] = useLocalStorage('fields_view_type', 'list');

  return (
    <div>
      <div className="header">
        <h3>Fields</h3>
        <div className="switch">
          <IconButton active={viewType === 'list'} onClick={() => setViewType('list')}>
            <MaterialIcon name="view_headline" />
          </IconButton>
          <IconButton active={viewType === 'table'} onClick={() => setViewType('table')}>
            <MaterialIcon name="view_list" />
          </IconButton>
        </div>
      </div>

      {viewType === 'list' ? <FieldsListView fields={fields} /> : <FieldsTableView fields={fields} />}

      {/* language=CSS */}
      <style jsx>{`
        .header {
          display: flex;
          align-items: center;
          justify-content: space-between;
        }

        h3 {
          margin: 0.75rem 0;
        }
      `}</style>
    </div>
  );
};
