import React from 'react';
import { Link } from 'react-router-dom';
import css from 'styled-jsx/css';

import Container from './Container';
import Button from './Button';
import TelegramIcon from '../images/telegram.svg';

const telegramIconStyles = css.resolve`
  svg {
    position: relative;
    top: 0.1em;
    margin-right: 0.5em;
  }
`;

export default () => (
  <div className="header-wrapper">
    <Container>
      <header>
        <div className="left">
          <span>
            <Link to="/">
              <TelegramIcon className={telegramIconStyles.className} />
              Telegram Bot API Console
            </Link>
            <span className="badge">Unofficial</span>
          </span>
        </div>
        <div className="right">
          <Button>Set token</Button>
        </div>
      </header>
    </Container>

    <style jsx>{`
      .header-wrapper {
        position: fixed;
        top: 0;
        left: 0;
        z-index: 10;
        width: 100%;
        background: #fff;
      }
      header {
        height: 3rem;
        padding: 0 0.5rem;
        border-bottom: 0.06rem solid #eee;
      }
      header,
      header > .left,
      header > .right {
        display: flex;
        align-items: center;
        justify-content: space-between;
      }

      .badge {
        margin-left: 0.5rem;
        color: #aaa;
        font-size: 0.8rem;
        font-weight: 500;
        text-transform: uppercase;
      }
    `}</style>
    {telegramIconStyles.styles}
  </div>
);
