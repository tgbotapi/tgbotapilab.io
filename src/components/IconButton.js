import React from 'react';
import classNames from 'classnames';
import Spinner from './Spinner';

export default ({
  children,
  primary,
  color,
  active,
  fullWidth,
  loading,
  ...other
}) => (
  <>
    <button
      className={classNames(primary && 'primary', loading && 'loading')}
      disabled={loading}
      {...other}
    >
      {loading ? <Spinner /> : children}
    </button>
    {/* language=CSS */}
    <style jsx>{`
      button {
        font: inherit;
        line-height: 0;
        border: none;
        border-radius: 100%;
        padding: 0.25em 0.25em 0.25em;
        background: #fff;
        cursor: pointer;
        transition: background-color 0.15s ease, color 0.15s ease;
      }
      button.loading {
        opacity: 0.7;
        pointer-events: none;
      }

      button:hover {
        background: rgba(0, 0, 0, 0.05);
      }
      button:active {
        background: rgba(0, 0, 0, 0.1);
      }
      button:focus {
        outline: none;
      }

      button.primary {
        background: rgb(100, 100, 200);
        color: #fff;
      }
      button.primary:hover {
        background: rgb(110, 110, 210);
        color: #fff;
      }
      button.primary:active {
        background: rgb(70, 70, 170);
        color: #eee;
      }
    `}</style>
    {/* language=CSS */}
    <style jsx>{`
      button {
        color: ${color ? color : active ? 'rgb(100, 100, 200)' : '#666'};
      }
    `}</style>
  </>
);
