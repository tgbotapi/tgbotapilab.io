import React from 'react';

export default ({ children, ...other }) => (
  <div {...other}>
    {children}

    {/* language=CSS */}
    <style jsx>{`
      div {
        padding: 1rem;
        padding-left: 0.5rem;
      }
    `}</style>
  </div>
);
