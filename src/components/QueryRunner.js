import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import Button from './Button';
import DescriptionView from './DescriptionView';

export default ({ method, fields }) => {
  const [queryData, setQueryData] = useState({});
  const [loading, setLoading] = useState(false);
  const [result, setResult] = useState(null);

  const handleChange = name => ev => {
    const value = ev.target.value.trim();
    setQueryData(data => ({
      ...data,
      [name]: value,
    }));
  };

  return (
    <div className="root">
      <div className="fields">
        {Object.keys(fields).map(fieldName => (
          <div key={fieldName} className="field">
            <label htmlFor={`input_${fieldName}`}>
              <h4>
                <code>{fieldName}</code>
                {fields[fieldName].required && (
                  <span className="required-mark">*</span>
                )}
              </h4>
              <p className="subtitle">
                {[]
                  .concat(
                    ...fields[fieldName].types
                      .map(field =>
                        field[0].toUpperCase() === field[0] ? (
                          <Link key={field} to={`/types/${field}`}>
                            {field}
                          </Link>
                        ) : (
                          <span key={field}>{field}</span>
                        )
                      )
                      .map((e, i) => [
                        <span key={i} className="joiner">
                          {' '}
                          or{' '}
                        </span>,
                        e,
                      ])
                  )
                  .slice(1)}
              </p>
            </label>
            <input
              id={`input_${fieldName}`}
              type="text"
              required={fields[fieldName].required}
              disabled={loading}
              onChange={handleChange(fieldName)}
              value={queryData[fieldName] || ''}
              spellCheck={false}
            />
            <div className="description">
              <DescriptionView description={fields[fieldName].description} />
            </div>
          </div>
        ))}
        <div className="button-wrapper">
          <Button primary fullWidth loading={loading}>
            Run
          </Button>
        </div>
      </div>

      <div className="result">
        <code>
          {result ? (
            JSON.stringify(result, null, 2)
          ) : (
            <i>Fill the fields and run the query.</i>
          )}
        </code>
      </div>

      {/* language=CSS */}
      <style jsx>{`
        .root {
          display: flex;
          align-items: stretch;
        }

        .fields {
          flex-shrink: 0;
          width: 15rem;
          padding: 1rem 0;
          padding-right: 1rem;
          border-right: 0.06rem solid #eee;
        }

        .result {
          flex: 1;
          padding: 1rem 0;
          padding-left: 1rem;
          font-size: 0.9rem;
        }
        .result i {
          opacity: 0.5;
        }

        .field {
          margin-bottom: 1rem;
        }

        input {
          width: 100%;
          padding: 0.25rem 0.75rem;
          border: none;
          border-radius: 0.25rem;
          font: inherit;
          font-size: 0.95rem;
          box-shadow: #ddd 0 0 0 0.06rem;
          transition: box-shadow 0.15s ease;
        }
        input:focus {
          outline: none;
          box-shadow: rgb(100, 100, 200) 0 0 0 0.12rem;
        }

        label h4 .required-mark {
          margin-left: 0.25rem;
          color: #f33;
        }

        h4 {
          margin-bottom: 0;
        }
        .subtitle {
          margin-top: 0;
          font-size: 0.9em;
        }
        .subtitle .joiner {
          color: #888;
        }

        .description {
          color: #888;
          font-size: 0.9rem;
          line-height: 1.4;
        }

        .button-wrapper {
          position: sticky;
          bottom: 0;
          padding: 0.5rem 0;
          border-top: 0.06rem solid #eee;
          background: #fff;
        }
      `}</style>
    </div>
  );
};
