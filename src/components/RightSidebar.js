import React, { useState } from 'react';
import { NavLink } from 'react-router-dom';
import IconButton from './IconButton';
import MaterialIcon from './MaterialIcon';

export default ({ types, methods }) => {
  const [searchQuery, setSearchQuery] = useState('');

  const filteredTypes = Object.keys(types).filter(name =>
    name.toLowerCase().includes(searchQuery.toLowerCase())
  );
  const filteredMethods = Object.keys(methods).filter(name =>
    name.toLowerCase().includes(searchQuery.toLowerCase())
  );

  const categories = {};

  Object.keys(types).forEach(typeName => {
    const type = types[typeName];
    const category = type.category;
    if (!categories[category]) categories[category] = {};
    if (!categories[category].types) categories[category].types = {};
    categories[category].types[typeName] = type;
  });

  Object.keys(methods).forEach(methodName => {
    const method = methods[methodName];
    const category = method.category;
    if (!categories[category]) categories[category] = {};
    if (!categories[category].methods) categories[category].methods = {};
    categories[category].methods[methodName] = method;
  });

  return (
    <div className="right-sidebar-wrapper">
      <div className="right-sidebar">
        <div className="search-field">
          <label htmlFor="searchQueryField">
            <MaterialIcon name="search" dark />
          </label>
          <input
            id="searchQueryField"
            type="text"
            placeholder="Search..."
            spellCheck={false}
            value={searchQuery}
            onChange={ev => setSearchQuery(ev.target.value.trim())}
          />
          {searchQuery !== '' && (
            <IconButton onClick={() => setSearchQuery('')}>
              <MaterialIcon name="close" dark />
            </IconButton>
          )}
        </div>

        {searchQuery !== '' ? (
          <ul>
            <li className="heading">Types</li>
            {filteredTypes.length ? (
              filteredTypes.map(name => (
                <NavLink
                  key={name}
                  to={`/types/${name}`}
                  className="item-wrapper"
                  activeClassName="active"
                >
                  <li className="item" title={name}>
                    {name}
                  </li>
                </NavLink>
              ))
            ) : (
              <li className="item">
                <i>Nothing found</i>
              </li>
            )}
            <li className="separator" />
            <li className="heading">Methods</li>
            {filteredMethods.length ? (
              filteredMethods.map(name => (
                <NavLink
                  key={name}
                  to={`/methods/${name}`}
                  className="item-wrapper"
                  activeClassName="active"
                >
                  <li className="item" title={name}>
                    {name}
                  </li>
                </NavLink>
              ))
            ) : (
              <li className="item">
                <i>Nothing found</i>
              </li>
            )}
          </ul>
        ) : (
          <ul>
            {Object.keys(categories).map((category, i) => (
              <React.Fragment key={category}>
                <li className="heading">{category}</li>
                {categories[category].types && Object.keys(categories[category].types).map(name => (
                  <NavLink
                    key={name}
                    to={`/types/${name}`}
                    className="item-wrapper"
                    activeClassName="active"
                  >
                    <li className="item" title={name}>
                      {name}
                    </li>
                  </NavLink>
                ))}
                {categories[category].methods && Object.keys(categories[category].methods).map(name => (
                  <NavLink
                    key={name}
                    to={`/methods/${name}`}
                    className="item-wrapper"
                    activeClassName="active"
                  >
                    <li className="item" title={name}>
                      {name}
                    </li>
                  </NavLink>
                ))}
                {i < Object.keys(categories).length - 1 && (
                  <li className="separator" />
                )}
              </React.Fragment>
            ))}
          </ul>
        )}
      </div>
      {/* language=CSS */}
      <style jsx>{`
        .right-sidebar-wrapper {
          position: absolute;
          right: 15rem;
        }

        .right-sidebar {
          position: fixed;
          top: 3rem;
          bottom: 0;
          width: 15rem;
          min-width: 15rem;
          max-height: 100%;
          border-left: 1px solid #eee;
          scrollbar-width: none;
          overflow-y: auto;
        }
        .right-sidebar::-webkit-scrollbar {
          display: none;
        }
        .right-sidebar:hover li {
          overflow: visible;
        }

        .search-field {
          display: flex;
          align-items: center;
          position: sticky;
          top: 0;
          padding: 0.4rem 0.6rem;
          border-bottom: 1px solid #eee;
          background: #fff;
        }

        .search-field label {
          line-height: 0;
        }

        .search-field input {
          flex: 1;
          margin-left: 0.5rem;
          border: none;
          background: none;
          color: inherit;
          font: inherit;
          font-size: 0.9rem;
          line-height: 2rem;
          transition: opacity 0.3s ease;
        }
        .search-field input:placeholder-shown {
          opacity: 0.75;
        }
        .search-field input:focus {
          outline: none;
          opacity: 1;
        }

        ul {
          padding: 1rem 0;
        }

        ul li {
          list-style: none;
          padding: 0.3rem 0.8rem;
        }

        ul li.item {
          color: #444;
          font-weight: 400;
          font-size: 0.9rem;
          overflow: hidden;
          text-overflow: ellipsis;
          cursor: pointer;
          transition: background-color 0.15s ease;
        }
        ul > :global(.item-wrapper) {
          text-decoration: none;
        }
        ul li.item:hover {
          background: #fafafa;
        }
        ul > :global(.active) li.item {
          background: rgb(100, 100, 200);
          color: #fff;
          font-weight: 500;
        }
        ul li.item i {
          opacity: 0.5;
        }

        ul li.heading {
          color: #888;
          font-size: 0.8rem;
          font-weight: 500;
          letter-spacing: 0.025rem;
          text-transform: uppercase;
        }

        ul li.separator {
          margin: 0.75rem 0;
          padding: 0;
          height: 0.06rem;
          background: #eee;
        }
      `}</style>
    </div>
  );
};
