import { useEffect } from 'react';
import { withRouter } from 'react-router-dom';

export default withRouter(({ location: { pathname }, children }) => {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, [pathname]);

  return children || null;
});
