import React from 'react';

export default () => (
  <div>
    <ul>
      <li className="item">Test</li>
      <li className="item">Test</li>
      <li className="item">Test</li>
      <li className="separator" />
      <li className="heading">Test heading</li>
      <li className="item">Test</li>
      <li className="item">Test</li>
      <li className="item">Test</li>
      <li className="separator" />
      <li className="item">Test</li>
      <li className="item">Test</li>
      <li className="item">Test</li>
      <li className="separator" />
      <li className="item">Test</li>
      <li className="item">Test</li>
      <li className="item">Test</li>
      <li className="separator" />
      <li className="item">Test</li>
      <li className="item">Test</li>
      <li className="item">Test</li>
      <li className="separator" />
      <li className="item">Test</li>
      <li className="item">Test</li>
      <li className="item">Test</li>
      <li className="separator" />
      <li className="item">Test</li>
      <li className="item">Test</li>
      <li className="item">Test</li>
      <li className="separator" />
      <li className="item">Test</li>
      <li className="item">Test</li>
      <li className="item">Test</li>
      <li className="separator" />
      <li className="item">Test</li>
      <li className="item">Test</li>
      <li className="item">Test</li>
    </ul>
    {/* language=CSS */}
    <style jsx>{`
      div {
        position: fixed;
        top: 3rem;
        flex-shrink: 0;
        width: 12rem;
        max-height: 100%;
        padding: 1rem 0;
        border-right: 1px solid #eee;
        scrollbar-width: none;
        overflow-y: auto;
      }
      div::-webkit-scrollbar {
        display: none;
      }
      
      ul li {
        list-style: none;
      }
      
      ul li.item {
        padding: 0.4rem 0.6rem;
        font-size: 0.9rem;
        cursor: pointer;
        transition: background-color .15s ease;
      }
      ul li.item:hover {
        background: #fafafa;
      }
      
      ul li.heading {
        color: #888;
        font-size: 0.8rem;
        font-weight: 500;
        letter-spacing: 0.025rem;
        text-transform: uppercase;
      }
      
      ul li.separator {
        margin: 0.75rem 0;
        padding: 0;
        height: 0.06rem;
        background: #eee;
      }
    `}</style>
  </div>
)
