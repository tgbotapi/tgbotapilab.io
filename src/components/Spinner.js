import React from 'react';

export default ({ size = 1.5, width = 1, color = 'inherit', ...other }) => (
  <>
    <i className="spinner" {...other} />
    <style jsx>{`
      .spinner {
        display: inline-block;
        font-size: ${typeof size === 'string' ? size : `${size}em`};
        color: ${color};
        width: 1em;
        height: 1em;
      }
      .spinner:after {
        content: ' ';
        display: block;
        width: 1em;
        height: 1em;
        border-radius: 50%;
        border: ${0.1 * width}em solid;
        border-bottom-color: transparent;
        animation: spinner-rotate 0.6s linear infinite;
      }
      @keyframes spinner-rotate {
        0% {
          transform: rotate(0deg);
        }
        100% {
          transform: rotate(360deg);
        }
      }
    `}</style>
  </>
);
