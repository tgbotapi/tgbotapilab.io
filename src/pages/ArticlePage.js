import React from 'react';
import PageContainer from '../components/PageContainer';
import NotFoundPage from './NotFoundPage';
import ReactMarkdown from 'react-markdown';

export default ({
  match: {
    params: { article: articleID },
  },
  articles,
}) => {
  const article = articles[articleID];

  if (!article) return <NotFoundPage />;

  return (
    <PageContainer>
      <h1>{article.title}</h1>
      <article>
        <ReactMarkdown source={article.markdown} />
      </article>
    </PageContainer>
  );
};
