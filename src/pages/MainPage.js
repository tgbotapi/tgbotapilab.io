import React from 'react';
import { Link } from 'react-router-dom';
import PageContainer from '../components/PageContainer';

export default ({ articles, botAPIVersion, buildInfo }) => {
  return (
    <PageContainer>
      <h1>Unofficial Telegram Bot API test console</h1>
      <p className="subtitle">
        Using Bot API schema v{botAPIVersion}
        <a
          className="schema-timestamp"
          href={buildInfo.pipeline_url}
          target="_blank"
          title={`commit ${buildInfo.commit}`}
        >
          {new Date(buildInfo.timestamp * 1000).toLocaleString()}
        </a>
      </p>

      <p>
        This web application provides Bot API documentation{' '}
        <s>with ability to run requests</s> <b>(work in progress!)</b> right in
        your browser. It uses{' '}
        <a href="https://gitlab.com/tgbotapi/schema" target="_blank">
          tgbotapi documentation parser
        </a>{' '}
        JSON scheme.
      </p>
      <p className="disclaimer">
        This app is unofficial and isn't related to Telegram FZ-LLC in any way.
      </p>

      <h2>Official Telegram documentation articles</h2>
      <ul className="articles-list">
        {Object.keys(articles).map(id => (
          <li key={id}>
            <Link to={`/articles/${id}`}>{articles[id].title}</Link>
          </li>
        ))}
      </ul>

      <style jsx>{`
        .subtitle {
          color: #666;
          font-size: 1.2em;
        }

        .schema-timestamp {
          margin-left: 0.5em;
          font-size: 0.8em;
          opacity: 0.5;
          transition: opacity 0.1s ease;
        }
        .schema-timestamp:hover {
          opacity: 1;
        }

        .disclaimer {
          font-weight: 500;
        }

        .articles-list,
        .articles-list li {
          list-style: none;
        }
        .articles-list li {
          position: relative;
          margin: 0.5em 0;
        }
        .articles-list li:before {
          content: '# ';
          position: absolute;
          left: -1em;
          font-weight: 600;
          color: #aad;
        }
      `}</style>
    </PageContainer>
  );
};
