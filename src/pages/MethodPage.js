import React from 'react';

import DescriptionView from '../components/DescriptionView';
import QueryRunner from '../components/QueryRunner';

import NotFoundPage from './NotFoundPage';
import PageContainer from '../components/PageContainer';

export default ({ match, methods }) => {
  const name = match.params.method;
  const method = methods[match.params.method];

  if (!method) return <NotFoundPage />;

  return (
    <PageContainer>
      <h1>{name}</h1>
      <DescriptionView description={method.description} />

      <QueryRunner method={name} fields={method.arguments} />
    </PageContainer>
  );
}
