import React from 'react';

import TypeFieldsView from '../components/FieldsView';
import DescriptionView from '../components/DescriptionView';

import NotFoundPage from './NotFoundPage';
import PageContainer from '../components/PageContainer';

export default ({ match, types }) => {
  const name = match.params.type;
  const type = types[match.params.type];

  if (!type) return <NotFoundPage />;

  return (
    <PageContainer>
      <h1>{name}</h1>
      <DescriptionView description={type.description} />

      <TypeFieldsView fields={type.fields} />
    </PageContainer>
  );
}
