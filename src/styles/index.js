import css from 'styled-jsx/css';

// language=CSS
export default css.global`
  @import url('https://fonts.googleapis.com/css?family=Roboto:400,400i,500,500i,700,700i|Roboto+Mono:400,700&display=swap&subset=cyrillic');
  @import url('https://fonts.googleapis.com/css?family=Material+Icons');

  *,
  *:before,
  *:after {
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    margin: 0;
    padding: 0;
    -webkit-appearance: none;
  }

  html {
    font-size: 16px;
  }

  body {
    font-family: 'Roboto', sans-serif;
    font-size: 1rem;
    line-height: 1.6;
    color: #333;
  }

  p {
    margin: 0.5em 0;
  }

  a {
    color: rgb(100, 100, 200);
    font-weight: 500;
    text-decoration: none;
    cursor: pointer;
  }
  a:hover {
    text-decoration: underline;
  }

  pre,
  code {
    font-family: 'Roboto Mono', monospace;
  }
  article > pre,
  article > p > code {
    background: #f8f8f8;
    color: #555;
  }
  article > pre {
    margin-left: -1rem;
    padding: 0.75rem 1rem;
    overflow-x: auto;
  }
  article > p > code {
    margin: 0 0.25em;
    padding: 0.4em 0.6em;
    font-size: 0.9em;
  }

  h1,
  h2,
  h3,
  h4,
  h5,
  h6 {
    font-weight: 500;
    margin-top: 0.75em;
    margin-bottom: 0.25em;
  }
  h2:first-child,
  h3:first-child,
  h4:first-child,
  h5:first-child,
  h6:first-child {
    margin-top: 0;
  }
  h1:first-child {
    margin-top: 0.5rem;
  }
  h1:last-child,
  h2:last-child,
  h3:last-child,
  h4:last-child,
  h5:last-child,
  h6:last-child {
    margin-bottom: 0;
  }

  h1 {
    font-size: 1.75rem;
  }
  h6 {
    font-size: 0.9rem;
    text-transform: uppercase;
    color: #888;
  }
`;
